using DeOnibusTeste.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Xunit;

namespace DeOnibusTesteTests
{
    public class UnitTest1
    {       
        private string apiUrl = "https://4jehkg0izj.execute-api.us-east-1.amazonaws.com/stage-v0/route";

        [Fact]
        public async void ApiUrlValid()
        {
            using var client = new HttpClient();

            client.BaseAddress = new Uri(apiUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync(apiUrl);

            Assert.True(response.IsSuccessStatusCode);
        }
        
        [Fact]
        public async void ApiReturnsSomething()
        {
            using var client = new HttpClient();

            client.BaseAddress = new Uri(apiUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync(apiUrl);
            var resp = await response.Content.ReadAsStringAsync();

            Assert.True(resp.Length > 0);
        }

        [Fact]
        public async void ApiReturnsTrips()
        {
            //Validar que API devolve JSON valido para Viagens
            using var client = new HttpClient();

            client.BaseAddress = new Uri(apiUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync(apiUrl);
            response.EnsureSuccessStatusCode();
            var resp = await response.Content.ReadAsStringAsync();

            Rootobject viagens = JsonConvert.DeserializeObject<Rootobject>(resp);

            Assert.True(viagens is Rootobject);
        }
       
}
}
