﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DeOnibusTeste.Models;
using System.Net.Http;
using System.Text.Json;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;

namespace DeOnibusTeste.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripsController : ControllerBase
    {
        private readonly Context _context;
        private readonly ILogger _logger;
        private readonly string ApiUrl = "https://4jehkg0izj.execute-api.us-east-1.amazonaws.com/stage-v0/route";
        public TripsController(Context context, ILogger<TripsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Trips/DeOnibusSave
        [HttpGet("DeOnibusSave")]
        public async Task<Rootobject> SaveTripsAPI()
        {         
            using var client = new HttpClient();

            client.BaseAddress = new Uri(ApiUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync(ApiUrl);
            response.EnsureSuccessStatusCode();
            var resp = await response.Content.ReadAsStringAsync();

            Rootobject viagens = JsonConvert.DeserializeObject<Rootobject>(resp);

            foreach (var item in viagens.results)
            {
                Random r = new Random();
                int num = r.Next();
                var itemSalvo = new Trip()
                {
                    Id = item.objectId,
                    Origin = item.Origin,
                    Price = item.Price,
                    updatedAt = item.updatedAt,
                    Destination = item.Destination,
                    createdAt = item.createdAt,
                    BusClass = item.BusClass,
                    ArrivalDate = new Arrivaldate()
                    {
                        id = num.ToString(),
                        __type = item.ArrivalDate.__type,
                        iso = item.ArrivalDate.iso
                    },
                    Company = new Company()
                    {
                        id = num.ToString(),
                        Name = item.Company.Name
                    },
                    DepartureDate = new Departuredate() { 
                        id = num.ToString(),
                        __type = item.DepartureDate.__type,
                        iso = item.DepartureDate.iso
                    },
                    Favorito = false
                };              

                _context.Trips.Add(itemSalvo);
                await _context.SaveChangesAsync();

            }
            _logger.LogInformation("Viagens da API DeOnibus Salvas as {0}", DateTime.Now);

            return viagens;
        }

        // GET: api/Trips/DeOnibus
        [HttpGet("DeOnibus")]
        public async Task<Rootobject> GetTripsAPI()
        {         
            using var client = new HttpClient();

            client.BaseAddress = new Uri(ApiUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync(ApiUrl);
            response.EnsureSuccessStatusCode();
            var json = await response.Content.ReadAsStringAsync();

            Rootobject viagens = JsonConvert.DeserializeObject<Rootobject>(json);
            _logger.LogInformation("Consulta das Viagens da API DeOnibus as {0}", DateTime.Now);

            return viagens;
        }

        // GET: api/Trips
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Trip>>> GetTrips()
        {
            _logger.LogInformation("Consulta das Viagens as {0}", DateTime.Now);

            return await _context.Trips.Where(x => x.Favorito == false).OrderByDescending(x => x.DepartureDate).ToListAsync();
        }

        // GET: api/Trips/Favoritos
        [HttpGet("Favoritos")]
        public async Task<ActionResult<IEnumerable<Trip>>> GetTripsFavoritos()
        {
            var trip = await _context.Trips.Where(x => x.Favorito == true).OrderByDescending(x => x.DepartureDate).ToListAsync();

            if (trip == null)
            {
                return NotFound();
            }
            _logger.LogInformation("Consulta das Viagens favoritas as {0}", DateTime.Now);

            return trip;
        }
        // GET: api/Trips/ListarPrecoMaior/{preco}
        [HttpGet("ListarPrecoMaior/{preco}")]
        public async Task<ActionResult<IEnumerable<Trip>>> GetTripsPrecoMaior(float preco)
        {
            var trip = await _context.Trips.Where(x => x.Price > preco).OrderByDescending(x => x.DepartureDate).ToListAsync();

            if (trip == null)
            {
                return NotFound();
            }
            _logger.LogInformation("Consulta das Viagens com preco maior a {0} as {1}", preco, DateTime.Now);

            return trip;
        }
        // GET: api/Trips/ListarPrecoMenor/{preco}
        [HttpGet("ListarPrecoMenor/{preco}")]
        public async Task<ActionResult<IEnumerable<Trip>>> GetTripsPrecoMenor(float preco)
        {
            var trip = await _context.Trips.Where(x => x.Price < preco).OrderByDescending(x => x.DepartureDate).ToListAsync();

            if (trip == null)
            {
                return NotFound();
            }
            _logger.LogInformation("Consulta das Viagens com preco menor a {0} as {1}", preco, DateTime.Now);

            return trip;
        }
        // PUT: api/Trips/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrip(string id, Trip trip)
        {
            if (id != trip.Id)
            {
                return BadRequest();
            }

            _context.Entry(trip).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TripExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            _logger.LogInformation("Salva viagem {0} via PUT as {1}", id, DateTime.Now);

            return NoContent();
        }

        // POST: api/Trips
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Trip>> PostTrip(Trip trip)
        {
            _context.Trips.Add(trip);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TripExists(trip.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            _logger.LogInformation("Salva viagem {0} via POST as {1}", trip.Id, DateTime.Now);


            return CreatedAtAction(nameof(GetTrips), new { id = trip.Id }, trip);
        }
        // GET: api/Trips/Favoritar/{id}
        [HttpGet("Favoritar/{id}")]
        public async Task<IActionResult> Favoritar(string id)
        {
            var trip = await _context.Trips.Where(x => x.Id == id).FirstOrDefaultAsync();

            if (trip == null)
            {
                return NotFound();
            }
            trip.Favorito = true;

            _context.Entry(trip).Property(x => x.Favorito).IsModified = true;

            await _context.SaveChangesAsync();
            _logger.LogInformation("Viagem {0} favoritada as {1}", id, DateTime.Now);

            return NoContent();
        }

        // DELETE: api/Trips/RemoverFavoritos
        [HttpDelete("RemoverFavoritos")]
        public async Task<IActionResult> DeleteFavorito()
        {
            var trip = _context.Trips;
            
            if (trip == null)
            {
                return NotFound();
            }
            foreach (var item in trip)
            {
               await UpdateFavorito(item);
            }
            _logger.LogInformation("Viagens favoritadas limpas as {0}", DateTime.Now);

            return NoContent();
        }
        private async Task<bool> UpdateFavorito(Trip trip)
        {    
            _context.Trips.Attach(trip);

            trip.Favorito = false;           

            _context.Entry(trip).Property(x => x.Favorito).IsModified = true;

            await _context.SaveChangesAsync();

            _logger.LogInformation("Viagem {0} salva como Update as {1}", trip.Id, DateTime.Now);

            return true;
        }
        private bool TripExists(string id)
        {
            return _context.Trips.Any(e => e.Id == id);
        }
    }
}
