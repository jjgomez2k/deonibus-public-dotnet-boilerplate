﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeOnibusTeste.Models
{
    public class Rootobject
    {
        public Result[] results { get; set; }
    }

    public class Result
    {
        public string objectId { get; set; }
        public Company Company { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public Departuredate DepartureDate { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public float Price { get; set; }
        public string BusClass { get; set; }
        public Arrivaldate ArrivalDate { get; set; }
    }

    public class Company
    {
        public string id { get; set; }

        public string Name { get; set; }
    }

    public class Departuredate
    {
        public string id { get; set; }
        public string __type { get; set; }
        public DateTime iso { get; set; }
    }

    public class Arrivaldate
    {
        public string id { get; set; }

        public string __type { get; set; }
        public DateTime iso { get; set; }
    }
}
