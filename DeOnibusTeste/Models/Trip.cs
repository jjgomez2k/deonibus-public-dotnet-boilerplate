﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeOnibusTeste.Models
{
    public class Trip
    {
        public string Id { get; set; }
        public Company Company { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public Departuredate DepartureDate { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public float Price { get; set; }
        public string BusClass { get; set; }
        public Arrivaldate ArrivalDate { get; set; }
        public bool Favorito { get; set; }
        public Trip()
        {

        }
        public Trip(Trip trip)
        {
            Id = trip.Id;
            Origin = trip.Origin;
            Price = trip.Price;
            updatedAt = trip.updatedAt;
            Destination = trip.Destination;
            createdAt = trip.createdAt;
            BusClass = trip.BusClass;
            ArrivalDate = trip.ArrivalDate;
            Company = trip.Company;
            DepartureDate = trip.DepartureDate;
            DepartureDate.id = new Random().ToString();
            ArrivalDate.id = new Random().ToString();
            Company.id = new Random().ToString();
            Favorito = trip.Favorito;
        }
    }

   

}
